## Name
Ismail Nejjar
Vinay Sharma
## Table of contents
* Instructions for running
* Exercise 1: comments on the overloading of CreateSimulation function in ParticlesFactory class.
* Exercise 2: how compute objects are managed correctly in the python bindings?
* Exercise 3: Instruction for running mercury dynamics.
* Exercise 5: Compute error fot the trajectory of Mercury.
* Exercise 7: Instruction to launch optimization routine.
* Plot of the scaling factor with error for Mercury
### Instructions for running
##### Set up and compilation
1. Clone the repository
2. Make a build directory inside the project directory
```
$ mkdir build
```
3. Run cmake from the build directory
```
$ cd build/
$ cmake ..
$ make
```
4. Create a **dumps directory** inside the **build** directory
```
$ mkdir dumps
```
### Exercise 1: Comments on the overloading of CreateSimulation function in ParticlesFactory class.
This is done to set non-default **createComputes** attributes for the simulation. These non-default parameters can be conductivity, capacity etc. If no functor is given to **createSimulation**, **createDefaultComputes** is called to add default attributes depending on the type of the particle.
### Exercise 2: How compute objects are managed correctly in the python bindings?
The python bindings are created with the **shared pointers** which are smart pointers which delete the memory allocated as the pointer goes out of scope.
### Exercise 3: Instruction for running mercury dynamics.
After compilation run following in the terminal from the src directory.
```
$ python3 main.py 365 1 '../init.csv' planet 1
```
### Exercise 5: Compute error fot the trajectory of Mercury.
Error for mercury : 165.098
### Exercise 7: Instruction to launch optimization routine.
Initial guess for velocity is set as 1.0 by default.
Run the compute_error.py by following : 
```
$ python3 compute_error.py
```
### Plot of the scaling factor with error for Mercury
##### As can be observed the optimal scaling factor is 0.4.
![Class diagram](images/scale_factor.jpg)
