import numpy as np
import pandas as pd
import os
import argparse
from main import main

import matplotlib.pyplot as plt
from scipy.optimize import fmin
from functools import partial


import warnings
warnings.filterwarnings("ignore")

#Description of user-required arguments during run
parser = argparse.ArgumentParser(description='Scipy optimization')

parser.add_argument("--planet_name", type=str, default="mercury",
                    help='Selected Planet')

parser.add_argument("--init_guess", type=float, default=1.0,
                    help='Initial velocity')

parser.add_argument("--freq", type=int, default=1,
                    help='Frequency of simulation')

args = parser.parse_args()

def readPositions(planet_name,directory):
    """
    
    Read trajectory of a given planet, and return array
    
    """
    
    all_files = os.listdir(directory)
    positions = np.zeros((len(all_files),3))

    for i, file in enumerate(all_files):
        df = pd.read_csv(directory+'/'+file, delimiter=r"\s+",header=None)

        positions[i,:] = df.loc[df[df.columns[-1]] == planet_name,df.columns[:3]].to_numpy()
        
    return positions

def computeErrror(positions,positions_ref):
    """
    
    Compute the error out of two numpy trajectories.
    
    """

    error = np.linalg.norm(positions-positions_ref)
    return error


def generateInput(scale, planet_name, input_filename, output_filename):
    """
    
    Generates input file from a given input file but by scaling velocity of a given planet
    
    """
    
    df = pd.read_csv(input_filename, header=None, delim_whitespace=True, index_col=-1, comment="#") #set index as planete name
    df.loc[planet_name][3:6] = df.loc[planet_name][3:6]*scale #scale
    df.insert(len(df.columns), "planet_name", df.index, True) #switch planete name to last column
    df.to_csv(output_filename,sep=" ",header=None,index=None)


def launchParticles(input,nb_steps,freq):
    """
    
    Launch the particle code on an provided input
    
    """

    main(nb_steps, freq, input, 'planet', 1)
    
def runAndComputeError(scale,planet_name,input,nb_steps,freq):
    """
    
    Function that gives the error far a given scaling velocity factor of a given planet
    
    """

    output_filename = 'init_s.csv'
    generateInput(scale,planet_name,input,output_filename)
    
    launchParticles(output_filename, nb_steps, freq)
    
    positions_ref = readPositions(planet_name, '../trajectories')
    positions_scaled = readPositions(planet_name, 'dumps')
    
    error = computeErrror(positions_scaled, positions_ref)

    return error 

def ploting_solution(sol_x,sol_y):
    plt.scatter(sol_x, sol_y)
    plt.xlabel('Scaled factor')
    plt.ylabel('Error')
    plt.title('Optimal scaling factor for Mercury')
    plt.savefig("output.jpg")
    #plt.show()


class callback:  
    def __init__(self, x_0,error_0):
        self.sol_x = [x_0]
        self.sol_error = [error_0]
        
    def callback_store(self, xk,planet_name, input_file, nb_steps,freq):
        print(xk[0])
        self.sol_x.append(xk[0])
        self.sol_error.append(runAndComputeError(xk, planet_name, input_file, nb_steps,freq))

    def ploting(self):
        ploting_solution(self.sol_x,self.sol_error)

if __name__ == "__main__":

    print(args)

    x_0 = args.init_guess
    error_0 = runAndComputeError(x_0, args.planet_name, '../init.csv', 365, args.freq)
    print('The initial guess scale is : ',x_0,' The error is : ',error_0)
    function = callback(x_0,error_0)

    error = fmin(runAndComputeError, x_0, args=(args.planet_name, '../init.csv', 365, args.freq), callback=partial(function.callback_store, planet_name=args.planet_name, input_file='../init.csv', nb_steps=365, freq=args.freq))

    print('The solution is : ', error)
    function.ploting()