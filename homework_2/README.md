**Name :** 

Ismail Nejjar
Vinay Sharma

**Submission for Homework 2:**

**Example Usage:**
    
 *Building in Clion*

#step1 : Build the project in Clion in the src directory
		     
			 cd src

#step2 :Run the code in Clion with following configuration 

            P P "" 100 3--> Pi 
            (series printing for 100 iterations with frequency 3)

            P W  , 100 3--> Pi 
            (series printing for 100 iterations with frequency 3 and writes to .csv file)

*Alternatively,In the terminal*

#step 1: build by cmake 

    cd build

        cmake ..

#step 2: In the /build directory execute the following : 

In the terminal execute

    ./main P W , 200 10    
--> calculates Pi series for 200 iterations with frequency of 10 and writes to a .csv 
            file

#step 3:In the directory ../Homework_2/build checkout the .csv generated       

**Division of the work:**

In this course, we meet on one day a week and sit together to understand the tasks and build a CLION project. We share the code through Slack and keep talking on slack to update the codes.
We sit together again in the last day to integrate everything together.

*Difficulty*

We have found that it is not efficient to work this way, we know that git can solve this problem, however, in order to do the excersizes on time we have hesitated to use git extensively. The final integration part, although seemed initially to us less time consuming, is infact more time-consuming as we face bugs in the integration. However, on the days we managed to find more time and communication through slack it works fine.

**Files and dependencies**

main.cpp

ComputeArithmetic.cpp

ComputeArithmetic.h : depends on "Series.h"

ComputePi.cpp

ComputePi.h : dependency "Series.h"

DumperSeries.h : dependency "Series.h"

PrintSeries.cpp

PrintSeries.h : dependency "DumperSeries.h"

Series.cpp

Series.h

WriteSeries.cpp

WriteSeries.h : dependency  "DumperSeries.h"







