#ifndef DumperSeries_H
#define DumperSeries_H
#include "Series.h"
#include <iostream>

class DumperSeries{
public:
    // constructor
    DumperSeries(Series & series);
    // members
    int precision;
    // abstract methods to implement
    virtual void dump(std::ostream & os)=0;
    virtual void setPrecision(unsigned int precision);
protected:
    // members
    Series & series;
};
inline std::ostream & operator <<(std::ostream & stream, DumperSeries & _this) {
    _this.dump(stream);
    return stream;
};
#endif
