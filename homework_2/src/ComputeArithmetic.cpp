/*
 * This file define the class-methods of the ComputeArithmetic class
 */
#include "ComputeArithmetic.h"
#include <cmath>

ComputeArithmetic::ComputeArithmetic() {};

double ComputeArithmetic::compute(unsigned int N){
    //check the index wrt given N
    if (this->current_index <= N){
        N -= this->current_index;
    }
    else{
        this->current_value = 0;
        this->current_index = 0;
    };
    // add term to members using addTerm method
    for (int k = 0; k < N; k++){
        this->add2series();
    }

    return this->current_value;

};
// method to update current term and serie value
void ComputeArithmetic::add2series(){
    this->current_index += 1;
    this->current_value += this->getseriesval(this->current_index);
};
// method to pull the terms of the series
float ComputeArithmetic::getseriesval(int k){
    return k;
};
// method to obtain the analytical prediciton, NaN for diverging Arithmetic serie
double ComputeArithmetic::getAnalyticPrediction() {
    return nan("1");
}
