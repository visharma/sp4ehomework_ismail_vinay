#ifndef PrintSeries_H
#define PrintSeries_H
#include "DumperSeries.h"

class PrintSeries: public DumperSeries {
public:
    // constructor
    PrintSeries(int maxiter, int freq, Series &series);
    // members
    int maxiter;
    int freq;
    // methods
    void dump(std::ostream & os) override;
};

#endif
