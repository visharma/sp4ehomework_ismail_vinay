#ifndef WriteSeries_H
#include "DumperSeries.h"
#include <string>

class WriteSeries: public DumperSeries {
public:
    // constructor
    WriteSeries(int maxiter, Series &series);
    // members
    int maxiter;
    std::string delimiter;
    std::string file_name;
    enum fileType {csv , psv, txt};
    // methods
    void dump(std::ostream & os) override;
    void setDelimiter(std::string delimiter);

};

#endif

