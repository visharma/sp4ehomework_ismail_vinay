#include "WriteSeries.h"
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

WriteSeries::WriteSeries(int maxiter, Series &series):DumperSeries(series){
    this->maxiter = maxiter;
};

void WriteSeries::dump(std::ostream & os){
    // create empty string which will store the value to be written
    std::string out;
    // for each iteration, calculate necessary values, store them in the string
    for (int i = 1; i < this->maxiter ; i++) {

        //calculate values for the iteration and the previous state for comparison
        double res = this->series.compute(i-1);
        double res2 = this->series.compute(i);

        //create strings to store the values of the series and the difference with previous step
        std::stringstream r1;
        std::stringstream r2;
        r1 << std::scientific << std::setprecision(this->precision) << res;
        r2 << std::scientific << std::setprecision(this->precision) << res2 - res;

        //store the calculated values on the empty string created
        out += std::to_string(i) + this->delimiter;
        out += r1.str() + this->delimiter;
        out += r2.str() + this->delimiter;


    }
    /*
    // create ofstream object, open it, write the store values in it and close
    std::ofstream datafile;
    datafile.open(this->file_name);
    datafile << out << std::endl;
    datafile.close();
    */
    // cast the ostream object to ofstream, open, write and close
    std::ofstream& file_stream = dynamic_cast<std::ofstream&>(os);
    file_stream.open(this->file_name);
    file_stream << out << std::endl;
    file_stream.close();
};
void WriteSeries::setDelimiter(std::string delimiter) {
    // USER INPUT TO ENUM
    fileType fileT;
    if (delimiter == ",") fileT = csv;
    else if (delimiter == "|") fileT = psv;
    else fileT = txt;

    //create necessary filename for writing the data depending on the requested file type
    switch(fileT){
        case (csv):{
            this->file_name = "..//build//series_data.csv";
            this->delimiter = delimiter;
            break;
        }
        case (psv):{
            this->file_name = "..//build//series_data.psv";
            this->delimiter = delimiter;
            break;
        }
        default:{
            this->file_name = "..//build//series_data.txt";
            this->delimiter = " ";
            break;
        }
    }
}

