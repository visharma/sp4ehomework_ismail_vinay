#include "PrintSeries.h"
#include <string>
#include <iomanip>
#include <iostream>

PrintSeries::PrintSeries(int maxiter, int freq, Series &series):DumperSeries(series){
    this->maxiter =maxiter;
    this->freq = freq;
};

void PrintSeries::dump(std::ostream & os = std::cout) {
    // create empty string which will store the value to be written
    std::string out;
    // for each iteration, calculate necessary values, store them in the string
    for (int i = 1; i < int(this->maxiter / this->freq); i++) {

        //calculate values for the iteration and the previous state for comparison
        double prev = this->series.compute(i * this->freq - 1);
        double current = this->series.compute(i * this->freq);

        //create strings to store the values of the series & Precision
        std::stringstream str1;
        std::stringstream str2;
        str1 << std::scientific << std::setprecision(this->precision) << prev;
        str2 << std::scientific << std::setprecision(this->precision) << current - prev;

        //store the value and the precision
        out += "\niter "+ std::to_string(i * this->freq) ;
        out += + "\tValue: " + str1.str() ;
        out += "\tPrecision: "+str2.str() ;


    }

    // create ofstream object, open it, write the store values in it and close
    std::cout << out << std::endl;
}
    //os << out << std::endl;

