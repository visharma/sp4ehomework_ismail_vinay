#ifndef ComputePi_H
#define ComputePi_H
#include "Series.h"

class ComputePi: public Series {
public:
    // constructor
    ComputePi();
    //methods
    double compute(unsigned int N) override;
    void add2series();
    float getseriesval(float k);
};

#endif

