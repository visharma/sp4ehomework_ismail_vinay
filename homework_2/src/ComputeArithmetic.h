#ifndef ComputeArithmetic_HH
#define ComputeArithmetic_HH
#include "Series.h"

class ComputeArithmetic: public Series {
public:
    // constructor
    ComputeArithmetic();
    // methods
    double compute(unsigned int N) override;
    void add2series();
    float getseriesval(int k);
    double getAnalyticPrediction() override;
};

#endif
