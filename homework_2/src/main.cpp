#include <iostream>
#include <string>
#include <memory>
#include "Series.h"
#include "ComputeArithmetic.h"
#include "ComputePi.h"
#include "PrintSeries.h"
#include "WriteSeries.h"
#include <fstream>
using namespace std;

int main(int argc, char ** argv) {

    // User chooses the type of series.
    std::string series_type = argv[1];
    std::string output_type = argv[2];
    //We set the delimiter of the file to write
    std::string delimiter= argv[3];
    //User Input the iterations
    auto maxiter = std::stoi(argv[4]);
    //User Input the frequencies
    auto freq = std::stoi(argv[5]);
    //EX: 2
    // INSTANTIATE SERIES CLASS WITH SERIES TYPE
    std::unique_ptr<Series> Series; //POINTER TO SERIES CLASS INSTANTIATION
    if (series_type == "A") {
        Series = std::make_unique<ComputeArithmetic>();
    } else if (series_type == "P") {
        Series = std::make_unique<ComputePi>();
    } else {
        std::cout << "Invalid Series Type Entered" << std::endl;
        return EXIT_FAILURE;
    }
    //EX: 3-4
    // CREATE OSTREAM OBJECT AND USE IN RELEVANT OUTPUT TYPE
    ofstream os;
    if (output_type == "W") {
        WriteSeries write_series(maxiter, *Series); //WriteSeries class is instantiated
        write_series.setPrecision(5);
        write_series.setDelimiter(delimiter);
        write_series.dump(os);
    } else if (output_type == "P") {
        PrintSeries print_series(maxiter, freq, *Series); //PrintSeries class in instantiated
        print_series.setPrecision(5);
        print_series.dump(os);
    } else {
        std::cout << "Invalid Output Type Entered" << std::endl;
        return EXIT_FAILURE;
    }

    return 0;
}
