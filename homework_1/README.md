# Homework week 4

Submission of the Homework by: Ismail Nejjar, Vinay Sharma

## Prerequisites
- Python 3
- Numpy
- Scipy
- Matplotlib

All the needed python libraries can be found in the requirements.txt 

## Run the code 

The program can be run with the default parameters using the following:

```python
python optimizer.py
```

Or by specifying the different parameters :  
```python
python optimizer.py --A 8 1 1 3 --A_row 2 --A_column 2 --b  2 4  --x_0  0 0  \ 
                    --method 'bfgs' --threshold 1e-32  ---iter 100  --ploting 1
```
The detailed list of parameters is : 

- ```--A``` : matrix for the linear system (n x n)
- ```--A_row``` : to specify the number of rows (n) of matrix A 
- ```--A_column``` : to specify the number of columns (n) of matrix A
- ```--b``` : vector from the linear system (n x 1)
- ```--x_0``` : Initial solution for the linear system (n x 1)
- ```--method``` : Solver method to be: bfgs , gmres, custom_gmres
- ```--threshold``` : Threshold for tolerance in solver
- ```--iter``` : Max iteration in solver 
- ```--ploting``` : Plotting the solution 0 for not, and 1 for yes

## Structure

The solution submission contains three files: `optimizer.py`, `GMRES.py`, and `ploting_utils.py` :

- `GMRES.py` is the file containing the computation of the GMRES method according to 
https://en.wikipedia.org/wiki/Generalized_minimal_residual_method. This file has three auxiliary functions 
```givens_rotation(), apply_given_rotations() and arnoldi()```. The main function is  ```gmres_solver()```.

- `ploting_utils.py` is the file containing the ```callback``` class as well as the ploting function: ```ploting_solution()``` .

- The main file is `optimizer.py` contains a function `s_x()` which takes $`A `$ , $`b `$ and $`x`$ and return $`S(x) = \frac{1}{2}x^TAx -x^Tb`$ as well as a ``` '__main__'```
with the different scipy optimization calls and the customized gmres solution. 

