import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def ploting_solution(sol_x,sol_y,A,b,foo):
    sol_x = np.array(sol_x)
    sol_y = np.array(sol_y)
    
    x = np.linspace(-3, 3, 100)
    y = np.linspace(-3, 3, 100)
    X, Y = np.meshgrid(x, y)
    Z = foo(np.array([X,Y]),A,b)
    
    fig = plt.figure(figsize=(6,6))
    ax = fig.add_subplot(111, projection='3d')
    
    ax.scatter(sol_x[:,0],sol_x[:,1],sol_y, c='r',s=100)
    ax.plot(sol_x[:,0],sol_x[:,1],sol_y, color='r')
    ax.plot_surface(X, Y, Z,cmap='viridis',alpha=0.3)

    ax.elev = 45
    ax.azim = 135  # xy view
    plt.show()

class callback:  
    def __init__(self, A,b,x_0,y_0,foo):
        self.A = A
        self.b = b
        
        self.sol_x = [x_0]
        self.sol_y = [y_0]

        self.foo = foo
        
    def callback_store(self, xk):
        self.sol_x.append(np.array(xk))
        self.sol_y.append(self.foo(xk,self.A,self.b))
        
    def ploting(self):
        ploting_solution(self.sol_x,self.sol_y,self.A,self.b,self.foo)
