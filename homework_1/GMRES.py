import numpy as np

def arnoldi(A,Q,k):
    q = np.einsum('ik,k->i',A,Q[:,k])
    h = np.zeros((k+2,))
    for i in range(0,k+1):
        h[i] = np.einsum('k,k->',q, Q[:,i])
        q = q - h[i]*Q[:,i]
    h[k+1] = np.sqrt(np.einsum('k,k->',q,q))
    q = q/h[k+1]
    return h,q

def givens_rotation(v1, v2):
    if (v1 == 0):
        cs = 0
        sn = 1
    else:
        t = np.sqrt(v1**2 + v2**2)
        cs = v1 / t
        sn = v2 / t
    return cs,sn

def apply_givens_rotation(h, cs, sn, k):
    for i in range(0,k):
        temp = cs[i]*h[i] + sn[i]*h[i+1]
        h[i+1] = -sn[i]*h[i] + cs[i]*h[i+1]
        h[i] = temp
        
    cs_k, sn_k = givens_rotation(h[k], h[k+1])
    h[k] = cs_k*h[k-1] + sn_k*h[k]
    h[k+1] = 0.0
    return h,cs_k,sn_k

def gmres_solver(A,b,x_0,callback,max_iter = 20,threshold = 1e-16,plotting=True):
    m = max_iter 
    n = A.shape[0]

    r = b - np.einsum('ik,k->i',A,x_0)
    r_norm = np.sqrt(np.einsum('k,k->',r,r))

    b_norm = np.sqrt(np.einsum('k,k->',b,b))

    error = r_norm / b_norm
    e = [error]

    # initialize the 1D vectors 
    sn = np.zeros(m)
    cs = np.zeros(m)
    e1 = np.zeros(m + 1)
    e1[0] = 1.0

    beta = r_norm * e1 

    H = np.zeros((m+1,m+1))
    Q = np.zeros((n, m+1))
    Q[:,0] = r / r_norm

    for k in range(m):

        (H[0:k+2, k], Q[:, k+1]) = arnoldi(A, Q, k)

        (H[0:k+2, k], cs[k], sn[k]) = apply_givens_rotation(H[0:k+2, k], cs, sn, k)

        beta[k+1] = -sn[k] * beta[k]
        beta[k] =  cs[k] * beta[k]

        error = abs(beta[k+1]) / b_norm
        e = np.append(e, error)
        if(error <= threshold):
            print(error)
            break
            
    for i in range(0,k+1):        
        y = np.linalg.lstsq(H[:i, :i], beta[:i],rcond=-1)[0]
        x = x_0 + np.einsum('ik,k->i',Q[:,:i],y)
        callback(x)

    return x