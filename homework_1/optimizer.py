import numpy as np
import argparse

from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres

from GMRES import gmres_solver
from ploting_utils import callback

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import warnings
warnings.filterwarnings("ignore")

#Description of user-required arguments during run
parser = argparse.ArgumentParser(description='Scipy optimization')
parser.add_argument('--A', type=int, nargs='+', default= [8,1,1,3], 
                    help="Matrix A")   #default :: 8 1 1 3
parser.add_argument('--A_row', type=int, default=2, 
                    help='Number of row of matrix A') #default 2 
parser.add_argument('--A_column', type=int, default=2, 
                    help='Number of column of matrix A') #default 2
parser.add_argument('--b', type=int, nargs='+', default= [2,4], 
                    help="Vector b") #default 2 4
parser.add_argument('--x_0', type=int, nargs='+', default= [0,0], 
                    help="Initial x") #default 0 0
parser.add_argument('--method', type=str, default='bfgs',
                    help='Solver method to be : bfgs , gmres, custom_gmres')
parser.add_argument('--threshold', type=float, default=1e-32,
                    help="Threshold for tolerance in solver") 
parser.add_argument('--iter', type=int, default=100,
                    help="Max iteration in solver")
parser.add_argument('--ploting', type=int, default=1,
                    help="Plotting the solution : 0 for not, and 1 for yes")
args = parser.parse_args()


def s_x(vec_x,A,b):
    return np.einsum('i...,ij,j...',vec_x,A,vec_x)/2 - np.einsum('i...,i',vec_x,b) 

if __name__ == '__main__':
    #Parameters A,b set from user input (default values in case no input) stored in args 
    A = np.array(args.A).reshape((args.A_row,args.A_column))
    b = np.array(args.b).reshape((args.A_row,))
    #variable x initialized 
    x_0 = np.array(args.x_0).reshape((args.A_row,))
    
    #Initial value of function sx
    y_0 = s_x(x_0,A,b) 

    function = callback(A,b,x_0,y_0,s_x)

    if(args.method == 'bfgs'):
        #scipy minimize
        x = minimize(s_x ,x_0 ,args=(A,b) ,callback=function.callback_store ,tol=args.threshold).x
    elif(args.method == 'gmres'):
        #scipy gmres
        x,_ = lgmres(A ,b ,x_0 ,callback= function.callback_store)
    elif(args.method == 'custom_gmres'):
        #custom gmres
        x = gmres_solver(A ,b ,x_0 ,callback=function.callback_store ,max_iter=args.iter ,threshold=args.threshold)
    else:
        print('Incorrect solver please use bfgs or gmres or custom_gmres')
        exit()

    print('The solution is : ', x)
    if(args.ploting):
        function.ploting()
