import numpy as np
import argparse

def intialize_variables(size):
    x = np.zeros((size, size)).flatten()
    y = np.zeros((size, size)).flatten()
    z = np.zeros((size, size)).flatten()
    return x,y,z

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("--particles",   type = int, default = 512, action = "store", help = "Grid of particles")
    parser.add_argument("--radius",  type = float, default=0.4, action = "store", help = "Heat Distribution within a provided Radius")
    parser.add_argument("--Halfdomain",  type = float, default=1, action = "store", help = "HalfDomain (x,y) in the interval")
    parser.add_argument("--filename", type = str, default = 'radial_heat.csv', action = "store", help="name of the generated file")

    args = parser.parse_args([])
    
    #define space 
    delta = 2*args.Halfdomain/args.particles
    x = np.linspace(-args.Halfdomain, args.Halfdomain, args.particles, endpoint=True)
    y = np.linspace(args.Halfdomain, -args.Halfdomain, args.particles, endpoint=True)

    # matrix of positions
    X, Y = np.meshgrid(x, y)
    Z = np.zeros((args.particles, args.particles))

    #Temperature definition and heat distribution 
    T = np.ones((args.particles, args.particles))*1

    HS = np.zeros((args.particles, args.particles))
    HS[np.where((X**2 + Y**2) < args.radius**2)] = 1.0
    
    velocity_x,velocity_y,velocity_z = intialize_variables(args.particles)
    f_x,f_y,f_z = intialize_variables(args.particles)
    
    masses = np.zeros((args.particles, args.particles))

    x = X.flatten()
    y = Y.flatten()
    z = Z.flatten()
    

    
    boundary = np.zeros((args.particles, args.particles), dtype=np.int32)
    boundary[0,:] = 1
    boundary[-1, :] = 1
    boundary[:, -1] = 1
    boundary[:, 0] = 1

    T[0, :] = 0
    T[-1, :] = 0
    T[:, -1] = 0
    T[:, 0] = 0

    t = T.flatten()
    hs = HS.flatten()
    masses = masses.flatten()
    boundary = boundary.flatten()

    #print results to output file
    data = np.column_stack((x,y,z,velocity_x,velocity_y,velocity_z,f_x,f_y,f_z,masses,t,hs,boundary))
    np.savetxt(args.filename, data, delimiter=" ", fmt='%1.3f')