#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */
void ComputeTemperature::setDeltaT(Real dt) {this->dt=dt;}
void ComputeTemperature::compute(System& system) {
    Real rho = 1.0; //mass_density
    Real C = 1.0; //specific_heat_capacity
    Real k = 1.0; //thermal_conductivity
    Real L; //Total length of domain

    UInt num_par = system.getNbParticles();
    UInt N = sqrt(num_par);

    /*We read each particle and form a grid of particles
     *We find the boundaries and impose boundary conditions*/
    Real left_boundary = 0.;
    Real right_boundary = 0.;
    Real top_boundary = 0.;
    Real bottom_boundary = 0.;
    for (auto& particle: system){
        Real x_coord =particle.getPosition()[0];
        Real y_coord = particle.getPosition()[1];
        if (x_coord>right_boundary)
            right_boundary = x_coord; // i=N-1
        if (x_coord<left_boundary)
            left_boundary = x_coord; //i=0
        if (y_coord>top_boundary)
            top_boundary=y_coord; //j=0
        if (y_coord<bottom_boundary)
            bottom_boundary=y_coord; //j=N-1
    }


    //Length of the domain
    L = right_boundary-left_boundary;
    //sampling width ds=dx=dy
    Real ds = (right_boundary-left_boundary)/(N-1);

    //Declaring matrices of theta and hv
    Matrix<complex> theta(N);
    Matrix<complex> hv(N);

    //Populating the theta and hv matrices
    for (auto& particle : system) {//loop over particles
        int i,j;
        auto& materialpoint = dynamic_cast<MaterialPoint&>(particle);
        Real x = materialpoint.getPosition()[0];
        Real y = materialpoint.getPosition()[1];
        i = round((x-left_boundary)/ds);
        j = round((top_boundary-y)/ds);
        // Fill the theta and hv matrices with particle's temperature and heat rate
        theta(i,j) = materialpoint.getTemperature();
        hv(i,j) = materialpoint.getHeatRate();
    }

    //D.F.T calculation
    Matrix<complex> theta_hat = FFT::transform(theta);
    Matrix<complex> hv_hat = FFT::transform(hv);

    //Frequency calculation
    Matrix<std::complex<int>> freq = FFT::computeFrequencies(N);

    //Declaring the matrix for time derivative of theta_hat(freq. domain)
    Matrix<complex> d_theta_hat_dt(N);
    //Assigning each element of matrix --> R.H.S of the Partial Diff. eq.
    for (auto&& entry : index(d_theta_hat_dt)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        double freq_squared = pow(freq(i,j).real(),2) + pow(freq(i,j).imag(),2);
        double coord_fourier_space =pow(2*M_PI/L,2)*freq_squared;
        val = ( 1.0 / (rho * C) ) * ( hv_hat(i,j) - k * theta_hat(i,j)*coord_fourier_space) ;
    }

    /*Inverse transforming the time_derivative of theta_hat(freq domain)
     *& get the time_derivative of theta in time domain.*/
    Matrix<complex> d_theta_dt = FFT::itransform(d_theta_hat_dt);

    //Evolve the temperature and assign to each particle
    for (auto& particle : system) {
        int i,j;
        auto& materialPoint = dynamic_cast<MaterialPoint&>(particle);
        Vector pos = materialPoint.getPosition();
        Real x = pos[0];
        Real y = pos[1];
        if (materialPoint.getBndCond()!=1){
            i = (int) ((x - left_boundary) / ds);
            j = (int) ((top_boundary - y) / ds);
            materialPoint.getTemperature() += dt * std::real(d_theta_dt(i, j));
        }
    }

}
