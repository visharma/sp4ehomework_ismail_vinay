#ifndef __MATERIAL_POINT__HH__
#define __MATERIAL_POINT__HH__

/* -------------------------------------------------------------------------- */
#include "particle.hh"

//! Class for MaterialPoint
class MaterialPoint : public Particle {
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:

  void printself(std::ostream& stream) const override;
  void initself(std::istream& sstr) override;

  Real & getTemperature(){return temperature;};
  Real & getHeatRate(){return heat_rate;};

  /* for adding boundary condition*/
  UInt & getBndCond(){return boundary_cnd;};
  
private:
  Real temperature;
  Real heat_rate;
  UInt boundary_cnd;
};

/* -------------------------------------------------------------------------- */
#endif  //__MATERIAL_POINT__HH__
