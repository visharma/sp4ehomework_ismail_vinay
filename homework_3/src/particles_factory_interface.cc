#include "particles_factory_interface.hh"
#include "planets_factory.hh"
/* -------------------------------------------------------------------------- */
ParticlesFactoryInterface& ParticlesFactoryInterface::getInstance() {
  return *factory;
}

/* -------------------------------------------------------------------------- */
//set the pointer to the factory instance <static> as a null-pointer(initialization)
ParticlesFactoryInterface* ParticlesFactoryInterface::factory = nullptr;
