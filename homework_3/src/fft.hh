#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

    static Matrix<complex> transform(Matrix<complex>& m);
    static Matrix<complex> itransform(Matrix<complex>& m);

    static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
    fftw_cleanup();
    UInt N = m_in.size();

    //Declare fftw plan, input and output vectors
    fftw_plan p;
    fftw_complex *in;
    fftw_complex *out;
    //Initialize the in and out matrices
    in =(fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N * N);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N * N);

    //Initialize plan :: FORWARD TRANSFORM
    p = fftw_plan_dft_2d(N,
                         N,
                         in,
                         out,
                         FFTW_FORWARD,
                         FFTW_ESTIMATE);

    //Assignment operations for filling "in" vector with "m_in" (input-matrix) values
    for (auto&& entry : index(m_in)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto val_in = std::get<2>(entry);
        auto k = i + j * N;
        in[k][0] = std::real(val_in);
        in[k][1] = std::imag(val_in);
    }

    //execution of plan p, writes DFT values to the "out" vector
    fftw_execute(p);

    //Declare the output matrix
    Matrix<complex> m_out(N);

    //Assignment operations for filling "m_out" with "out" values
    for (auto&& entry : index(m_out)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val_out = std::get<2>(entry);
        auto k = i + j * N;
        val_out = *out[k];
    }

    // Memory clean up operations
    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);
    fftw_cleanup();

    return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
    UInt N = m_in.size();

    //Declare fftw plan, input and output vectors
    fftw_plan p;
    fftw_complex *in;
    fftw_complex *out;

    //Initialize the in and out matrices
    in =(fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N*N);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N*N);

    //Initialize plan :: BACKWARD TRANSFORM
    p = fftw_plan_dft_2d(N,
                         N,
                         in,
                         out,
                         FFTW_BACKWARD,
                         FFTW_ESTIMATE);

    //Assignment operations for filling "in" vector with "m_in" (input-matrix) values
    for (auto&& entry : index(m_in)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto val_in = std::get<2>(entry);
        auto k = i + j * N;
        in[k][0] = std::real(val_in)/(N*N); //normalized result
        in[k][1] = std::imag(val_in)/(N*N); //normalized result
    }

    //execution writes DFT values to the "out" vector
    fftw_execute(p);

    //Declare the output matrix
    Matrix<complex> m_out(N);

    //Assignment operations for filling "m_out" with "out" values
    for (auto&& entry : index(m_out)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val_out = std::get<2>(entry);
        auto k = i + j * N;
        val_out = *out[k];
    }

    // Memory clean up operations
    fftw_destroy_plan(p);
    fftw_free(*in);
    fftw_free(*out);
    fftw_cleanup();

    return m_out;
}

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
    //Calculates the frequencies similar to np.fft.fftfreq

    /* Documentation for np.fft.fftfreq(n,d=1.0)
     * Given a window length n and a sample spacing d:
     * f = [0, 1, ...,   n/2-1,     -n/2, ..., -1] / (d*n)   if n is even
     * f = [0, 1, ..., (n-1)/2, -(n-1)/2, ..., -1] / (d*n)   if n is odd
     *
     * For 2D DFT, there are frequencies in columns and rows
     * Since only one matrix is required to be returned we decide to
     * return at each entry in matrix a resultant freq= sqrt((freqx**2+freqy**2))
     * */

    //declare the Freq vector
    std::vector<int> Freq(size*size);

    //Determine the size of sub-series of frequencies with positive and negative vals.
    int size_PositiveFreq;
    if (size % 2){  // if N is odd
        size_PositiveFreq = (size-1)/2+1;
    }else{ // N is even
        size_PositiveFreq = size/2;
    }
    int size_NegFreq = size-size_PositiveFreq;

    //Declare the sub-series vectors for positive and negative frequencies
    std::vector<int> Freq_1st_half(size_PositiveFreq);
    std::vector<int> Freq_2nd_half(size_NegFreq);

    //assign positive frequencies to vector
    for (int i = 0; i < size_PositiveFreq; i++){
        Freq_1st_half[i] = i; // odd->[0, 1, ..., (n-1)/2]; even-> [0, 1, ..., (n)/2]
    }

    //assign negative frequencies (-1*reverse of positive series)
    for (int i = 0; i <=size_NegFreq; i++){
        Freq_2nd_half[i] = -(i+1); // odd->[0, 1, ..., (n-1)/2]; even-> [0, 1, ..., (n)/2]
    }

    //Freq vector initialized to the vector of positive frequencies
    Freq = Freq_1st_half;
    //append the freq_2nd_half vector to the freq vector in reverse order
    Freq.insert(Freq.end(),Freq_2nd_half.rbegin(),Freq_2nd_half.rend());

    //declare a complex matrix for returning the frequencies
    Matrix<std::complex<int>> out(size);

    //Assigning the row freq to real and column-freq to imaginary part of elements
    for (auto&& entry : index(out)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto k = i + j * size;
        auto& v = std::get<2>(entry);
        v = std::complex<int>(Freq[i],Freq[j]);
    }
    return out;
}
#endif  // FFT_HH
