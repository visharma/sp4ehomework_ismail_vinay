#include "compute_temperature.hh"
#include "my_types.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "system.hh"
#include <gtest/gtest.h>
#include <cmath>

/*********************************************************************/
/*Make sure that dumps folder exits in build folder, as output will
 * also be dumped to a csv for visualization*/
/*********************************************************************/

//Fixture class for tests
class RandomGridFactory : public ::testing::Test {
protected:
    void SetUp() override {
        // Initialization of grid params
        L = 2.; //width and height
        num_particles =128;
        dt = 1e-6; //time_step
        Real ds = L/(num_particles); //distance between each particle

        //add particles to the system owned by the instance of simulation 'evol'
        for (UInt j = 0; j < num_particles; j++) {
            for (UInt i = 0; i < num_particles; i++) {
                auto mat_pt = MaterialPointsFactory::getInstance().createParticle();
                mat_pt->getPosition()[0] = i*ds - L/2.; //x_coord
                mat_pt->getPosition()[1] = L/2.-j*ds;
                evol->getSystem().addParticle(std::move(mat_pt));
            }
        }

        //Creation of temperature computation
        temperature = std::make_shared<ComputeTemperature>();
        //setting time_step
        temperature->setDeltaT(dt);
        //add compute to the simulation
        evol->addCompute(temperature);
    }
    //members
    std::unique_ptr<SystemEvolution> evol =
            std::make_unique<SystemEvolution>(std::make_unique<System>()); //instance of simulation
    UInt num_particles;
    Real L;
    Real dt;
    std::shared_ptr<ComputeTemperature> temperature;
};
/*****************************************************************/

TEST_F(RandomGridFactory, homogeneous_temp) {
    Real initial_temperature=1.;
    UInt n_time_steps=10;

    //1.add the material point to system
    //2.initialize the temperature of each particle according to sinosoidal distribution
    //3.initialize heat_rate to sinosoidal heat rate
    for (auto &pt : evol->getSystem()) {
        auto &mat_pt = dynamic_cast<MaterialPoint&>(pt);
        mat_pt.getTemperature() = initial_temperature;
        mat_pt.getHeatRate() = 0.;
    }
    //setup_simulation
    evol->setNSteps(n_time_steps);
    evol->setDumpFreq(5);

    //run_simulation
    evol->evolve();

    //Googletest assertions :: Temperature should not change
    for (auto &pt : evol->getSystem()){
        auto &mat_pt = dynamic_cast<MaterialPoint&>(pt);
        ASSERT_NEAR(mat_pt.getTemperature(), initial_temperature, 1e-15);
    }
}
/*****************************************************************/

TEST_F(RandomGridFactory, vol_heat_sinusoidal) {
    UInt n_time_steps=50;

    //1.add the material point to system
    //2.initialize the temperature of each particle according to sinosoidal distribution
    //3.initialize heat_rate to sinosoidal heat rate
    for (auto &pt : evol->getSystem()) {
        double x = pt.getPosition()[0];
        auto &mat_pt = dynamic_cast<MaterialPoint&>(pt);
        mat_pt.getTemperature()=sin(2*M_PI*x/L);
        mat_pt.getHeatRate()=pow((2*M_PI)/L,2)*sin(2*M_PI*(x/L));
    }

    //setup_simulation
    evol->setNSteps(n_time_steps);
    evol->setDumpFreq(10);

    //run_simulation
    evol->evolve();

    /*Googletest assertions :: Temperature should not change since equilibrium
     T is assigned as initial T*/
    for (auto &pt : evol->getSystem()){
        auto &mat_pt = dynamic_cast<MaterialPoint&>(pt);
        double x = mat_pt.getPosition()[0];
        ASSERT_NEAR(mat_pt.getTemperature(), sin(2*M_PI*x/L), 1e-5);
    }
}
/*****************************************************************/

TEST_F(RandomGridFactory, vol_heat_step) {
    UInt n_time_steps=50;

    //1.add the material point to system
    //2.initialize the temperature of each particle according to sinosoidal distribution
    //3.initialize heat_rate to sinosoidal heat rate
    for (auto &pt : evol->getSystem()) {
        double x = pt.getPosition()[0];
        auto &mat_pt = dynamic_cast<MaterialPoint&>(pt);

    //Initial temperature
        if (x <= -0.5) {
            mat_pt.getTemperature() = -x - 1.0;
        }
        else if (x <= 0.5) {
            mat_pt.getTemperature() = x;
        }
        else if (x > 0.5) {
            mat_pt.getTemperature() = -x + 1.0;
        }

    //Heat rate assignment
        if (x==0.5){
            mat_pt.getHeatRate() = 1;
        }else if (x==-1*0.5){
            mat_pt.getHeatRate() = -1;
        }else{
            mat_pt.getHeatRate() = 0;
        };
    }

    //setup_simulation
    evol->setNSteps(n_time_steps);
    evol->setDumpFreq(10);

    //run_simulation
    evol->evolve();

   //Googletest assertions :: Temperature should not change since equilibrium T is assigned as initial T
    for (auto &pt : evol->getSystem()){
        auto &mat_pt = dynamic_cast<MaterialPoint&>(pt);
        double x = mat_pt.getPosition()[0];
        if (x <= -0.5) {
            ASSERT_NEAR(-x-1, mat_pt.getTemperature(), 1e-5);
        } else if (x > -0.5 and x <=0.5 ) {
            ASSERT_NEAR(x,  mat_pt.getTemperature(), 1e-5);
        } else if (x > 0.5){
            ASSERT_NEAR(-x+1.0, mat_pt.getTemperature(), 1e-5);
        }
    }
}
